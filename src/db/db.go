package db

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	//	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	product "protobase/src/product"
	pb "protobase/src/protocol/src"

	"time"
)
func GetContext() *context.Context{
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	return &ctx
}
//let's get a client pointer to use it with our grpc service
func GetClient(uri *string) (*mongo.Client, error){
	clientOptions := options.Client().ApplyURI(*uri)
	clientOptions.SetAuth(options.Credential{
		AuthSource: "admin", Username: "root", Password: "example",
	})
	client, err := mongo.NewClient(clientOptions)


	if err != nil {
		log.Println("GetClient got mad: ", err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	err = client.Connect(ctx)
	if err != nil {
		log.Println("GetClient Connect kaboom: ", err)
	}
	//defer client.Disconnect(ctx)
	return client, err
}

//insert or update products with a batch
func ItemsUpsert(collection *mongo.Collection, items []*product.Item){

	var operations []mongo.WriteModel


	for _, item := range items{
		operationA := mongo.NewUpdateOneModel()
		item.Price = product.RoundPrice(item.Price, 0.05)
		log.Println("upsert item:", item.Name, item.Price)

		//filter out items that do not have their price changed?
		// or just update during the fetch? business logic question
		operationA.SetFilter(bson.M{"Name": item.Name,
			//"Price": bson.M{"$ne": product.RoundPrice(item.Price, 0.05),}
			})
		log.Println(product.RoundPrice(item.Price, 0.05))
		operationA.SetUpdate(
			bson.M{
				"$set": bson.M {
					"Name":item.Name,
					"Price": product.RoundPrice(item.Price, 0.05), //bad
					"Updated": time.Now(), // todo: timezone
				 },
				 "$inc": bson.M{"TimesChanged":1} })
		operationA.SetUpsert(true)
		operations = append(operations, operationA)
	}

	bulkOption := options.BulkWriteOptions{}
	bulkOption.SetOrdered(true)

	_, err := collection.BulkWrite(context.TODO(), operations, &bulkOption)
	if err != nil {
		log.Println("bulk upsert error:", err)
	}
}

func GetItems(params *pb.Params,
	collection *mongo.Collection,
	out chan product.Item,
)  {
	log.Println("GetItems", params)

	if params == nil || params.Paging == nil || params.Sorting == nil{
		log.Println("bad grpc request params")
		close(out)
		return
	}
	defer close(out)
	page := params.Paging.CurrentPage
	limit := params.Paging.ProductsPerPage
	sortBy := params.Sorting.By
	sortOrder := params.Sorting.Direction

	ctx := GetContext()

	filter := bson.D{}

	skip:= page * limit
	opts := options.FindOptions{
		Skip: &skip,
		Limit: &limit,
	}


	opts.SetSort(bson.M{sortBy:sortOrder})

	findOptions := opts

	cur, err := collection.Find(*ctx, filter, &findOptions)
	if err != nil {
		log.Println("Error during text search:", err)
		//return err
	}
	defer cur.Close(*ctx)

	for cur.Next(*ctx) {
		var p product.Data
		//var item bson.M
		if err := cur.Decode(&p); err != nil {
			log.Println("Error decoding mongo data:", err)
			continue
		}
		out <- product.Item{Data: p}
	}

	if err := cur.Err(); err != nil {
		log.Println(err)
	}


}


func CreateIndexOnProductName(collection *mongo.Collection){

	index := mongo.IndexModel{Keys: bson.M{"name": "text"}}
	ctx := GetContext()
	if _, err := collection.Indexes().CreateOne(*ctx, index); err != nil {
		log.Println("Index exists:", err)
	}
}