package fetcher

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

//func TestFetchFile(t *testing.T) {
//	data, err := FetchFile("http://strangeqargo.net/products.csv")
//	if err != nil {
//		t.Error("FetchFile: HTTP Gone mad")
//	}
//	fmt.Println(data)
//}

func TestGetCSVReader(t *testing.T) {
	data, err := FetchFile("http://strangeqargo.net/products.csv")

	r, err := GetCSVReader(&data)
	records, err := r.ReadAll()
	assert.NotEqual(t, len(records), 0, "No records found in csv")
	if err != nil {
		t.Errorf("GetCSVReader: CSV kicked a bucket, %v", err)
	}
	//fmt.Print(records)
}