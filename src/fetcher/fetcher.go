package fetcher

import (
	"encoding/csv"
	"io/ioutil"
	"net/http"
	"strings"
)
type Fetcher interface {
	FetchFile(url string) (*string, error)
	Parse(data *string)
}

func FetchFile(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	//Convert the body to type string
	sb := string(body)
	return sb, nil
}

func GetCSVReader(data *string) (*csv.Reader, error){
	c:=*data
	r := csv.NewReader(strings.NewReader(c))
	r.Comma = ';'
	if _, err := r.Read(); err != nil {
		return r, err
	}
	//records, err := r.ReadAll()
	//if err != nil {
	//	return r, err
	//}

	//fmt.Print(records)
	return r, nil

}
