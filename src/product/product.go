package product

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"math"
	"strconv"
	"strings"
	"time"

	//"protobase/src/protocol/src"
)

type Data struct {
	ID   primitive.ObjectID  `bson:"_id,omitempty"`
	Name string              `bson:"name"`
	Price  float64           `bson:"price"`
	TimesChanged    int32    `bson:"times_changed,omitempty"`
	Created    time.Time     `bson:"created,omitempty"` // can get this get from MongoID
	Updated    time.Time     `bson:"updated,omitempty"`
}


type Manager interface {
	NewProduct(name string, price float32) *Data
	validate(*Data) error
}

type Item struct {
	Data
}

type ItemError interface {
	error() string
}

type ItemPriceError struct {
	err ItemError
}
type ItemNameError struct {
	err ItemError
}

func (t ItemNameError) Error() string{
	return "Bad product name"
}

func (t ItemPriceError) Error() string{
	return "Bad product price"
}

func NewItem(name string, price string) (*Item, error) {
	fPrice, err := strconv.ParseFloat(price, 64)
	if err != nil {
		return  nil, err
	}
	p := Data{}
	p.Name = strings.Trim(name, " ") // people make errors naming things
	p.Price = RoundPrice(fPrice, 0.05)

	_, err = IsValid(&p)
	if err != nil{
		log.Print(err)
		return  nil, err
	}

	return &Item{p}, nil
}


func IsValid(data *Data) (bool, error) {
	if len(data.Name) >= 64{
		return false, ItemNameError{}
	}
	if (data.Price) >= 1000 || (data.Price) < 0 {
		return false, ItemPriceError{}
	}
	return true, nil
}

func RoundPrice(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}


type DefaultSelectParams struct {


}