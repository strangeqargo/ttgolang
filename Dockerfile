FROM golang:1.15


WORKDIR /go/src/protobase
COPY . .

ENV PATH="$PATH:$(go env GOPATH)/bin"
RUN apt update && apt install -y protobuf-compiler
RUN go get google.golang.org/protobuf/cmd/protoc-gen-go google.golang.org/grpc/cmd/protoc-gen-go-grpc;
RUN bash regen_proto.sh

RUN go get -d -v ./...; \
go get github.com/fullstorydev/grpcurl/... ;\
go install github.com/fullstorydev/grpcurl/cmd/grpcurl;
RUN go install -v server.go;

CMD ["server"]