package main

import (
	"context"
	"flag"
	"github.com/golang/protobuf/ptypes"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"go.mongodb.org/mongo-driver/mongo"
	"io"
	"protobase/src/db"
	//"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"protobase/src/fetcher"
	product "protobase/src/product"
	pb "protobase/src/protocol/src"
)

const (
	port = ":50051"
)

// server is used to implement protobase.ProtobaseServer.
type server struct {
	pb.UnimplementedProtobaseServer
	client *mongo.Client
	collection *mongo.Collection
	productsStore []*pb.Product
}


// Fetch implements protobase.ProtobaseServer
func (s *server) Fetch(ctx context.Context, in *pb.ProductListURL) (*pb.ProductFetchResult, error) {
	log.Printf("Received url: %v", in.Url)
	//@todo: check http cache validation, no need to update the database if the file has not changed
	cache_is_valid := false

	if cache_is_valid {
		return &pb.ProductFetchResult{Quantity: 0}, nil
	}
	//retrieve file
	data, err := fetcher.FetchFile(in.Url)
	log.Println(err)
	if err != nil{
		log.Println("No products fetched")
		return &pb.ProductFetchResult{Quantity: 0}, err
	}
	//get file parser
	csv, err := fetcher.GetCSVReader(&data)
	recordsLength := 0
	items := []*product.Item{}
	for {
		record, err := csv.Read()
		if err == io.EOF {
			break
		}
		if err != nil{
			log.Println(err)
			break
		}
		p, err := product.NewItem(record[0], record[1])
		items = append(items, p)
		if err != nil {
			break
		}
		log.Println(p)
		recordsLength += 1
	}


	db.ItemsUpsert(s.collection, items)

	return &pb.ProductFetchResult{Quantity: int32(recordsLength)}, nil
}

// ListProducts returns a stream of products
func (s *server) List(params *pb.Params, stream pb.Protobase_ListServer) error {
	//if params == nil {
	//	return errors.New("Bad params recieved")
	//}
	in := make(chan product.Item)
	go db.GetItems(params, s.collection, in)
	//if err != nil {
	//	log.Println("GetItems gone crazy", err)
	//	return err
	//}
	//defer s.mongoCtx.C
	for i := range in {
		pbProduct := pb.Product{}
		pbProduct.Name = i.Name
		log.Println("GetItems product",pbProduct.Name )
		// yes, DRY principle, I know
		created, err := ptypes.TimestampProto(i.ID.Timestamp())
		if err != nil {
			log.Println("Bad mongo timestamp for product id", created)
			continue
		}

		updated, err := ptypes.TimestampProto(i.Updated)
		if err != nil {
			log.Println("Bad mongo updated time for product id", updated)
			continue
		}

		{
			pbProduct.Name = i.Name
			pbProduct.Price = i.Price
			pbProduct.TimesChanged = i.TimesChanged
			pbProduct.Updated = updated
			pbProduct.Created = created
		}

		if err := stream.Send(&pbProduct); err != nil {
			return err
		}

	}

	return nil
}

var (
	customFunc grpc_recovery.RecoveryHandlerFunc
)


func main() {

	//products, _ := db.GetProductsCollection("localhost:27017")
	port:=flag.String("port", ":50051", "server port")
	mongo_uri:=flag.String("mongo", "mongodb://localhost:27017/protobase", "mongo uri")
	flag.Parse()

	mongoClient,  err := db.GetClient(mongo_uri)
	collection := mongoClient.Database("protobase").Collection("products")
	//mongoCtx := db.GetContext()
	db.CreateIndexOnProductName(collection)
	lis, err := net.Listen("tcp", *port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	//// panic
	//
	//customFunc = func(p interface{}) (err error) {
	//	return status.Errorf(codes.Unknown, "panic triggered: %v", p)
	//}
	//// Shared options for the logger, with a custom gRPC code to log level function.
	//opts := []grpc_recovery.Option{
	//	grpc_recovery.WithRecoveryHandler(customFunc),
	//}
	//// Create a server. Recovery handlers should typically be last in the chain so that other middleware
	//// (e.g. logging) can operate on the recovered state instead of being directly affected by any panic
	//s := grpc.NewServer(
	//	grpc_middleware.WithUnaryServerChain(
	//		grpc_recovery.UnaryServerInterceptor(opts...),
	//	),
	//	grpc_middleware.WithStreamServerChain(
	//		grpc_recovery.StreamServerInterceptor(opts...),
	//	),
	//)

	// no panic

	p := pb.Product{
		Name:  "",
		Price: 0,
	}
	productsStore := []*pb.Product{&p}

	pb.RegisterProtobaseServer(s, &server{
		productsStore: productsStore,
		client: mongoClient,
		collection: collection,
	})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

