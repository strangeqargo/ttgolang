#!/bin/bash

protoc --go_out=./src/protocol --go_opt=paths=source_relative \
    --go-grpc_out=./src/protocol --go-grpc_opt=paths=source_relative \
    src/products.proto