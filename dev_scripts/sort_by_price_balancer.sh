#!/bin/bash
# скрипт для проверки protobase.Protobase/List
# to test via docker nginx balancer: ./dev_scripts/fetch.sh 8080
cd "$(dirname "${BASH_SOURCE[0]}")" && source config.sh
if [ $1 ]; then
  DEV_PORT=$1
fi

#


#grpcurl -plaintext -msg-template localhost:${DEV_PORT} describe  protobase.Protobase.List
#grpcurl -plaintext -msg-template localhost:${DEV_PORT} describe  protobase.Params
cat sort_by_price.json


cat sort_by_price.json | docker exec --interactive  protobase2 grpcurl -d '@' -plaintext  protobase_balancer:8080 protobase.Protobase/List
