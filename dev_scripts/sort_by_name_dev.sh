#!/bin/bash
# скрипт для проверки protobase.Protobase/Fetch
# to test via docker nginx balancer: ./dev_scripts/fetch.sh 8080
cd "$(dirname "${BASH_SOURCE[0]}")" && source config.sh
if [ $1 ]; then
  DEV_PORT=$1
fi

#

#
#grpcurl -plaintext -msg-template localhost:${DEV_PORT} describe  protobase.Protobase.List
#grpcurl -plaintext -msg-template localhost:${DEV_PORT} describe  protobase.Params

cat sort_by_name.json | grpcurl -d '@' -plaintext localhost:${DEV_PORT}  protobase.Protobase/List