#!/bin/bash
# скрипт для проверки protobase.Protobase/ListProducts
# to test via docker nginx balancer: ./sort_by_name_balancer.sh 8080
cd "$(dirname "${BASH_SOURCE[0]}")" && source config.sh
if [ $1 ]; then
  DEV_PORT=$1
fi

#

cat sort_by_name.json | docker exec --interactive  protobase1 grpcurl -d '@' -plaintext  protobase_balancer:8080 protobase.Protobase/List
