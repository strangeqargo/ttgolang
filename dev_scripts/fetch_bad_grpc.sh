#!/bin/bash
# скрипт для проверки protobase.Protobase/Fetch
# to test via docker nginx balancer: ./dev_scripts/fetch.sh 8080
cd "$(dirname "${BASH_SOURCE[0]}")" && source config.sh
if [ $1 ]; then
  DEV_PORT=$1
fi

if [ $2 ]; then
  DEV_CSV_URL=$2
fi

#grpcurl -d '{"url":"localhost:/products.csv"}' -plaintext localhost:${DEV_PORT}  protobase.Protobase/Fetch
grpcurl -d '' -plaintext localhost:${DEV_PORT}  protobase.Protobase/Fetch
